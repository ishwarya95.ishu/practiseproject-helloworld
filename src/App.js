import React from 'react';
//nimport logo from './logo.svg';
import './App.css';
import Greet from './Components/Greet'
import Hello from './Components/Hello'

function App() {
  return (
    <div className="App">
      <Greet />
      <Hello />
    </div>
  );
}

export default App;
